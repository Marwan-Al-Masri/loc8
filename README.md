
![cover](https://user-images.githubusercontent.com/6648552/34352047-24a21748-ea5b-11e7-8bb4-e3b2a5297d92.png)

![Vesion](https://img.shields.io/badge/vesion-1.0-157575.svg?logo=app-store)
![Swift vesion](https://img.shields.io/badge/swift-5.2-orange.svg?logo=swift)
![Xcode vesion](https://img.shields.io/badge/xcode-11.3-blue.svg?logo=Xcode)
![Platforms](https://img.shields.io/badge/platforms-iOS|macOS-lightgray.svg?logo=apple)
[![Build Status](https://travis-ci.com/marwam-almasri/LOC8.svg?token=x3se2FtHpByNm6tPmCBc&branch=master)](https://travis-ci.com/marwam-almasri/LOC8)
[![odecov](https://codecov.io/gh/marwam-almasri/LOC8/branch/development/graph/badge.svg?token=TVGJOPNAJK)](https://codecov.io/gh/marwam-almasri/LOC8)
![Documentation](./docs/badge.svg)
[![Fastlane](./fastlane/fastlane-badge.svg)](https://docs.fastlane.tools/)

<!-- Development | Production
---|---
![graph](https://codecov.io/gh/marwan-almasri/LOC8/branch/development/graphs/sunburst.svg?token=g1kcxuoSG6) | ![graph](https://codecov.io/gh/marwan-almasri/LOC8/branch/master/graphs/sunburst.svg?token=g1kcxuoSG6) -->

# LOC8

 LOC8 is a mobile application project that detect a human motion in three dimensions.

## Requirements
- Swift 5
- Xcode 11.0 and above
- macOS 10.15 and above
- [Pod](https://cocoapods.org/) v1.6.0 and above - A package manager
- [Jazzy](https://github.com/realm/jazzy) - A code documentation generator
- [Swiftlint](https://github.com/realm/SwiftLint) - A Swift code linter & fixer
- [Fastlane](https://docs.fastlane.tools/) - A deployments and releases tool

## Get Started

install cocoapods
```bash
gem install cocoapods
```
> you might need `sudo` to install.

install swift lint
```bash
brew install swiftlint
```
install jazzy
```bash
gem install jazzy
```

install fastlane
```bash
# install Xcode command tools if needed
xcode-select --install

gem install fastlane - NV
```
> you might need `sudo` to install.

Finally open `LO*.xcworkspace` and start development.

## Documentation

you can access the project documentation [here](https://marwan-almasri.github.io/LOC8/index.html). before releasing make sure to generate the project documentation.

to generate documentation run `jazzy` in the terminal.

## Contributing
If you are new to this repository, please read [development doc](Documentation/Development.md) first.

## License
LOC8 Sdn Bhd
